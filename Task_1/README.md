
## JS - Fix the bug


Function appendChildren should add a new child div to each existing div. New divs should be decorated by calling decorateDiv.


## For example, after appendChildren is executed, the following divs:

```shell
<div id="a">
    <div id="b">
    </div>
  </div>
```


## should take the following form (assuming decorateDiv does nothing):
```shell
<div id="a">
    <div id="b">
      <div></div>
    </div>
    <div></div>
  </div>
```


## The code below should do the job, but for some reason it goes into an infinite loop. Fix the bugs.
```shell
function appendChildren() {
  var allDivs = document.getElementsByTagName("div");
  
  

  for (var i = 0; i < allDivs.length; i++) {
    var newDiv = document.createElement("div");
    decorateDiv(newDiv);
    allDivs[i].appendChild(newDiv);
  }
}

// Mock of decorateDiv function for testing purposes
function decorateDiv(div) {}
```




